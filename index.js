console.log("hello");

// create student one
let studentOneName = "John";
let studentOneEmail = 'john@mail.com';
let studentOneGrades = [89, 84, 78, 88];

// create student two
let studentTwoName = 'Joe';
let studentTwoEmail = 'joe@mail.com';
let studentTwoGrades = [89, 84, 78, 88];

// create student three
let studentThreeName = 'Jane';
let studentThreeEmail = 'jane@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

// create student four
let studentFourName = 'Jessie';
let studentFourEmail = 'jessie@mail.com';
let studentFourGrades = [91, 89, 92, 93];

function login(email) {
	console.log(`${email} has logged in`);

}

function logout(email) {
	console.log(`${email} has logged out`);
}

function listGrades(grades){
	grades.forEach(grade => {
		console.log(grade)
	})
}

// use an object literal: {}
// ENCAPSULATION - the organization of information (properties) and behavior (as methods) to belong to the object that encapsulates them (the scope of encapsulation is denoted by the object literals)

let studentOne = {
	name: 'John',
	email: 'john@mail.com',
	grades: [89, 84, 78, 88],

	// add the functionalities available to a student as object methods
		// the keyword "this" refers to the object encapsulating the method where "this" is called
	login(){
		console.log(`${this.email} has logged in`);
	},
	logout(){
		console.log(`${this.email} has logged out`);
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
	},

	computeAve(){
	let sum = 0;
	this.grades.forEach(grade => sum = sum + grade);
	return sum/4;
	},

	/* Mini-Exercise 2:
	Create a function that will return true if the average grade is >= 85, false if not
	*/
	willPass(){
		// hint: you can call methods inside an object
		// let AverageGrade = this.computeAve();
		// if (AverageGrade >= 85) return true
		// 	else return false;
		return this.computeAve() >= 85 ? true : false
	},
	/*
		Mini-Exercise 3
		Create a function called willPassWithHonors() that returns if the student has passed AND their average grade is >=90. The function reutn false if either one is not met.
	*/
	willPassWithHonors(){
		if (this.computeAve() >= 90) return true
			else if (this.computeAve() >= 85) return false
				else return undefined
	},
}

console.log(`student one's name is ${studentOne.name}`);
console.log(`student one's name is ${studentOne.email}`);
console.log(`student one's name is ${studentOne.grades}`);


/*
Mini-Exercise:
Create a function that will get/compute the quarterly average of the studentOne's grades:
*/

/*
Activity:
1. Spaghetti
2. []
3. encapsulation
4. this.enroll()
5. false
6. key: value
7. true
8. true
9. true
10. true
*/
// Coding Activity

let studentTwo = {
	name: 'Joe',
	email: 'joe@mail.com',
	grades: [89, 84, 78, 88],

	login(){
		console.log(`${this.email} has logged in`);
	},
	logout(){
		console.log(`${this.email} has logged out`);
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
	},
	computeAve(){
	let sum = 0;
	this.grades.forEach(grade => sum = sum + grade);
	return sum/4;
	},
	willPass(){
		return this.computeAve() >= 85 ? true : false
	},
	
	willPassWithHonors(){
		if (this.computeAve() >= 90) return true
			else if (this.computeAve() >= 85) return false
				else return undefined
	},
}

let studentThree = {
	name: 'Jane',
	email: 'jane@mail.com',
	grades: [87, 89, 91, 93],

	login(){
		console.log(`${this.email} has logged in`);
	},
	logout(){
		console.log(`${this.email} has logged out`);
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
	},
	computeAve(){
	let sum = 0;
	this.grades.forEach(grade => sum = sum + grade);
	return sum/4;
	},
	willPass(){
		return this.computeAve() >= 85 ? true : false
	},
	
	willPassWithHonors(){
		if (this.computeAve() >= 90) return true
			else if (this.computeAve() >= 85) return false
				else return undefined
	},
}

let studentFour = {
	name: 'Jessie',
	email: 'jessie@mail.com',
	grades: [91, 89, 92, 93],

	login(){
		console.log(`${this.email} has logged in`);
	},
	logout(){
		console.log(`${this.email} has logged out`);
	},
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
	},
	computeAve(){
	let sum = 0;
	this.grades.forEach(grade => sum = sum + grade);
	return sum/4;
	},
	willPass(){
		return this.computeAve() >= 85 ? true : false
	},
	
	willPassWithHonors(){
		if (this.computeAve() >= 90) return true
			else if (this.computeAve() >= 85) return false
				else return undefined
	},
}

// Activity: 5

let classOf1A = {
		students: [
			studentOne,
			studentTwo,
			studentThree,
			studentFour
			],
			countHonorStudents(){
				let count = 0;
				// this.willPassWithHonors().forEach(count => count = 1 + count);
				
				// return count;
				console.log(this.students.willPassWithHonors())
				

			}
		}
	

